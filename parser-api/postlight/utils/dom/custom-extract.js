export function getPucblicTime($) {
    try {
        const htmlCollectionCheck = [
            "head>meta[property='article:published_time']",
            "head>meta[name='pubdate']",
            "head>meta[name='date']",
            "head>meta[name='parsely-pub-date']",
            "head>meta[name='publication_date']",
            "head>meta[name='ailthru.date']",
            "head>meta[name='dcterms.created']",
            "head>meta[name='cXenseParse:recs:publishtime']",
            "head>meta[name='DCSext.articleFirstPublished']",
            "head>meta[property='story:published_time']",
            "head>meta[name='datePublished']",
            "head>meta[itemprop='datePublished']",
            "head>meta[name='Last-Modified']",
            "head>meta[itemprop='dateCreated']",
        ]
        let lengArr = htmlCollectionCheck.length;
        let i = 0
        while (i < lengArr) {
            const nodes = $(htmlCollectionCheck[i]);
            const values = nodes
                .map((index, node) => $(node).attr('content'))
                .toArray()
                .filter(text => text !== '');
            if (values.length === 1)
                return values[0]
            i++
        }

        let timeTmp = $('script[type="application/ld+json"]')
        if (timeTmp && timeTmp.length > 0)
            return JSON.parse(timeTmp[0].text).datePublished
    }
    catch (err) {
        return null;
    }

    return null;
}


export function getAuthor($) {
    try {
        const htmlCollectionCheck = [
            "head>meta[property='article:author']",
            "head>meta[name='article:author']",
            "head>meta[name='author']",
            "head>meta[property='article:author_name']",
            "head>meta[property='article:publisher']",
        ]
        let lengArr = htmlCollectionCheck.length;
        let i = 0
        while (i < lengArr) {
            const nodes = $(htmlCollectionCheck[i]);
            const values = nodes
                .map((index, node) => $(node).attr('content'))
                .toArray()
                .filter(text => text !== '');
            if (values.length === 1)
                return values[0]
            i++
        }

        let authorTmp = document.querySelector('script[type="application/ld+json"]')
        if (authorTmp && authorTmp.length > 0) {
            const jsonTmp = authorTmp.text
            if (Array.isArray(JSON.parse(authorTmp[0]).author))
                return typeof JSON.parse(authorTmp[0]).author[0] === "object" ? JSON.parse(authorTmp[0]).author[0].name : JSON.parse(authorTmp[0]).author[0]

            return JSON.parse(authorTmp[0]).author.name
        }
    }
    catch (err) {
        return null;
    }

    return null;
}