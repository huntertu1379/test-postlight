import cheerio from 'cheerio';

import { normalizeSpaces } from '../../../utils/text/index.js';

const getWordCount = content => {
  const $ = cheerio.load(content);
  const $content = $('div').first();
  const text = normalizeSpaces($content.text());
  return text.split(/\s/).length;
};

const getWordCountAlt = content => {
  function rp(content) {
    content = content.replace(/<[^>]*>/g, ' ');
    content = content.replace(/\s+/g, ' ');
    content = content.trim();
    return content.split(' ').length;
  }
  if (Array.isArray(content)) {
    return rp(content[0])
  }
  return rp(content)
};

const GenericWordCountExtractor = {
  extract({ content }) {
    let count = getWordCount(content);
    if (count === 1) count = getWordCountAlt(content);
    return count;
  },
};

export default GenericWordCountExtractor;
