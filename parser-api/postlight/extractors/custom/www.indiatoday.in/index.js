// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const IndiatodayExtractor = {
    domain: 'www.indiatoday.in',
    author: {
        selectors: ['.Story_story__author__cJoes']
    },
    date_published: null,
    content: {
        defaultCleaner: false,
        selectors: ['.main__content'],
        transforms: {
            '.Photo_maintitle__G_cdj': "b",
            '.Photo_kicker__pnqUP': "p",
            '.Photo_photoCard__list__F08Fh': "figure",
            '.PhotoCard_card__details__3Le4m': "figcaption",
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
        },
        clean: ['.Story_profile__pic__ASpda', '.Story_story__byline__7MVK3', '.PhotoCard_sharing__00iUZ.sharing', '.Story_sharing__1KH3a.sharing',
            '.article__type.PhotoCard_article__type__dNLO0', '.CommentForm_commentsform__edsSZ', 'aside', '.story-recommended-chunk',
            '.Story_authors__container__i9YKo', '.Story_storyfooter__dP5T3', '.tbl-read-more-box', '#shoppingWidgetLSH', '#shoppingWidget_mobile', '.Livetv_tabs__gkL3E'
            , '.Livetv_newscard__list__DJEOc', '.video__grid']
    },
};
