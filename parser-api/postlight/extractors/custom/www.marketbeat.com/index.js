// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const MarketbeatExtractor = {
    domain: 'www.marketbeat.com',
    author: null,
    date_published: null,
    content: {
        selectors: ['#articlecontent'],
        transforms: {
            '.headline-image': "figure",
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
        },
        clean: []
    },
};
