export const VietnamplusExtractor = {
    domain: 'en.vietnamplus.vn',

    author: null,

    date_published: null,

    content: {
        defaultCleaner: false,
        selectors: ['.content.article-body.cms-body'],

        transforms: {
        },
        clean: [],
    },
};
