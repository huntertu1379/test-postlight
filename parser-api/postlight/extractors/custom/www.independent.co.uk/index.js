// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const IndependentExtractor = {
  domain: 'www.independent.co.uk',
  author: {
    selectors: ['.news-article--author'],
  },
  date_published: {
    selectors: [['time[datetime]', 'datetime']],
  },
  content: {
    selectors: ['#main', '.videoTextContent'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      '#main': ($node, $) => {
        const nodeImg = $('#articleHeader>div>figure');
        if (nodeImg && nodeImg.length > 0) $node.prepend(nodeImg[0]);
      },
    },
    clean: ['div[data-newsletter-key]', 'interaction'],
  },
};
