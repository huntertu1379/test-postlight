// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const UsnewsExtractor = {
    domain: 'www.usnews.com',
    author: null,
    date_published: null,
    content: {
        selectors: ['section#main-column', 'section.article__CenterColumn-sc-1ng6nv6-3.iFXOJg',
            'div.Box-w0dun1-0.PhotoGalleryEmbed__AnimatedBox-lqudj4-0.bMEgtT.iGUSaK.PhotoGalleryEmbed-lqudj4-15.eSWtcX.PhotoGalleryEmbed-lqudj4-15.eSWtcX',
            'div.shared__SlideContainer-sc-1avpvy5-6.iSsZHO', 'div.Cell-sc-1abjmm4-0.kMnrRl'],
        transforms: {
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
            'react-trigger[trigger="view"]': $node => {
                const parent = $node.parents('div.Cell-sc-1abjmm4-0.kMnrRl')
                if (parent.length > 0) return;
                const img = $node.find('img')
                if (img.length == 0) $node.remove();
            }

        },
        defaultCleaner: false,
        clean: ['div.Box-w0dun1-0.drvHMZ', 'div.shared__CreditContainer-sc-1avpvy5-2.kieDTp',
            'div.Hide-kg09cx-0.hBzpiJ', 'div.Box-w0dun1-0.PhotoGalleryEmbed__GalleryWrap-lqudj4-6.dWWnRo.jaLmgV',
            'div.Box-w0dun1-0.dWWnRo.Hide-kg09cx-0.hBzpiJ.Hide-kg09cx-0.hBzpiJ',
            'div.Box-w0dun1-0.SlideshowEmbed__GalleryWrap-fkpjfn-3.dWWnRo.dQzrUM', 'div.SlideshowEmbed__ArrowWrapper-fkpjfn-6.fYcvrc',
            'div#country-ranking-details', 'div.Box-w0dun1-0.CountryCompare__Main-sc-1ls147v-2.dWWnRo.hQhLJL', 'div.mt5.Show-oz18v2-0.fSXhAI', '.md-hide', '.lg-hide'],
    },
};
