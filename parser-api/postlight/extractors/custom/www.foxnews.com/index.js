// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const FoxnewsExtractor = {
  domain: 'www.foxnews.com',
  author: {
    selectors: ['div.author-byline>span'],
  },
  date_published: {
    selectors: ['div.article-date>time'],
  },
  content: {
    selectors: ['div.article-body'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      'div.m.video-player': $node => {
        const videoId = $node.attr('data-video-id');
        const $videoContainer = $(
          `<script type="text/javascript" src="https://video.foxnews.com/v/embed.js?id=${videoId}&w=466&h=263"></script>`
        );
        $node.replaceWith($videoContainer);
      },
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: [],
  },

  dek: {
    selectors: [],
  },

  next_page_url: {
    selectors: [
      // enter selectors
    ],
  },

  excerpt: {
    selectors: [
      // enter selectors
    ],
  },
};
