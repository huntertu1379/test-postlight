// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const VarietyExtractor = {
  domain: 'variety.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['.vy-cx-page-content', '.a-content'],
    transforms: {
      '.vy-cx-page-content': ($node, $) => {
        const prepenNode = $('div.article-header__feature').find('figure');
        if (prepenNode && prepenNode.length > 0) $node.prepend(prepenNode[0]);
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: ['.story-card.manual', '.article--content-embed'],
  },
};
