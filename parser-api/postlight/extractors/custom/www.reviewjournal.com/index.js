// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ReviewjournalExtractor = {
  domain: 'www.reviewjournal.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['.entry-content'],
    transforms: {
      '.entry-content': ($node, $) => {
        const nodeImg = $('.rj-gallery-container').find(
          'div[data-xs-type="image"]>img'
        );
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
      iframe: $node => {
        $node.attr('width', '100%');
        $node.attr('height', '500px');
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
        $node.removeAttr('srcset');
      },
    },
    clean: ['.Mg2-connext.Mg2-inline', 'footer'],
  },
};
