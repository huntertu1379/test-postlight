// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const NewsExtractor = {
  domain: 'www.news.com.au',
  author: null,
  date_published: null,
  content: {
    selectors: ['#story-primary'],
    transforms: {
      '#story-primary': ($node, $) => {
        const nodeImg = $('#primary-media>.media.image').find('img');
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
      iframe: $node => {
        $node.attr('width', '100%');
        $node.attr('height', '500px');
      },
      '.media.image': 'figure',
    },
    clean: [],
  },
};
