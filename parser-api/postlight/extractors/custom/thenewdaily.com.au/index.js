export const ThenewdailyExtractor = {
  domain: 'thenewdaily.com.au',

  author: null,

  date_published: null,

  content: {
    defaultCleaner: false,
    selectors: [
      'div.tnd-content-style.tnd-content-style--article',
      'div.article__body-content',
      'div.article__main',
    ],
    transforms: {
      'div.tnd-content-style.tnd-content-style--article': ($node, $) => {
        const prepenNode = $('div.article-feature.article-feature--image').find(
          'img[width]'
        );
        if (prepenNode && prepenNode.length > 0) $node.prepend(prepenNode[0]);
        else {
          const prepenNode2 = $(
            'div.article-feature.article-feature--image'
          ).find('img');
          if (prepenNode2 && prepenNode2.length > 0)
            $node.prepend(prepenNode2[0]);
        }
      },
      'div.article__body-content': ($node, $) => {
        const prepenNode = $('div.article-feature.article-feature--image').find(
          'img[width]'
        );
        if (prepenNode && prepenNode.length > 0) $node.prepend(prepenNode[0]);
        else {
          const prepenNode2 = $(
            'div.article-feature.article-feature--image'
          ).find('img');
          if (prepenNode2 && prepenNode2.length > 0)
            $node.prepend(prepenNode2[0]);
        }
      },
    },
    clean: ['.tnd-content--type'],
  },
};
