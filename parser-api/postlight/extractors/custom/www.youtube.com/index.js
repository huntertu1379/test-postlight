export const WwwYoutubeComExtractor = {
  domain: 'www.youtube.com',

  title: {
    selectors: ['.watch-title', 'h1.watch-title-container'],
  },

  author: {
    selectors: [['link[itemprop="name"]', 'content']],
  },

  date_published: {
    selectors: [['meta[itemProp="datePublished"]', 'value']],

    timezone: 'GMT',
  },

  dek: {
    selectors: [
      // enter selectors
    ],
  },

  lead_image_url: {
    selectors: [['meta[name="og:image"]', 'content']],
  },

  content: {
    defaultCleaner: false,

    selectors: [['#player', '#expander>#content>#description']],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      '#player': ($node, $) => {
        let videoId = window.location.search.split('v=')[1];
        const ampersandPosition = videoId.indexOf('&');
        if (ampersandPosition != -1) {
          videoId = videoId.substring(0, ampersandPosition);
        }
        $node.html(
          `<iframe src="https://www.youtube.com/embed/${videoId}" frameborder="0" allowfullscreen  width="100%" height="350"></iframe>`
        );
      },
      'span.style-scope.yt-formatted-string': ($node, $) => {
        $node.attr('style', 'text-align: justify;');
        return 'p';
      },
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: ['#snippet', '#items', '#collapse'],
  },
};
