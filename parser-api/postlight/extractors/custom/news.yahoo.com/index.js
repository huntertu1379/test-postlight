// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const NewsYahooExtractor = {
  domain: 'news.yahoo.com',
  author: {
    selectors: ['span.caas-author-byline-collapse'],
  },
  date_published: {
    selectors: [['time', 'datetime']],
  },
  content: {
    selectors: ['div.caas-body'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      'div.caas-yvideo.caas-yvideo-rendered': $node => {
        const videoUrl = JSON.parse($node.attr('data-videoconfig')).url;
        const $videoContainer = $(
          `<iframe src="${videoUrl}" frameborder="0" allowfullscreen  width="100%" height="350"></iframe>`
        );
        $node.replaceWith($videoContainer);
      },
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: [],
  },

  dek: {
    selectors: [],
  },

  next_page_url: {
    selectors: [
      // enter selectors
    ],
  },

  excerpt: {
    selectors: [
      // enter selectors
    ],
  },
};
