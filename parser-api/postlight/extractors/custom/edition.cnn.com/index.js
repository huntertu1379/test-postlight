// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const EditionCnnExtractor = {
  domain: 'edition.cnn.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['.article__content', '.article__main'],
    transforms: {
      '.article__content': ($node, $) => {
        const nodeImg = $('.image__lede.article__lede-wrapper').find(
          'picture>img'
        );
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [],
  },
};
