// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ScotsmanExtractor = {
    domain: 'www.scotsman.com',
    author: null,
    date_published: null,
    content: {
        selectors: ['.article-content'],
        transforms: {
            'iframe': $node => {
                $node.attr('width', '100%');
                $node.attr('height', '500px');
            },
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
        },
        clean: ['i-amphtml-sizer','button'],
    },
};
