// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const HotcarsExtractor = {
  domain: 'www.hotcars.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['.w-article.infinite-scroll'],
    transforms: {
      '.heading_image': $node => {
        const nodeImg = $node.find('img');
        if (nodeImg && nodeImg.length > 0) $node.replaceWith(nodeImg[0]);
      },
      '.w-youtube': $node => {
        const nodeFrame = $node.find('iframe');
        if (nodeFrame && nodeFrame.length > 0) $node.replaceWith(nodeFrame[0]);
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    allowMultiple: true,
    clean: ['.related-single', 'footer', 'source'],
  },
};
