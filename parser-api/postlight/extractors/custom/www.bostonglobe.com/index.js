// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const BostonglobeExtractor = {
  domain: 'www.bostonglobe.com',
  author: null,
  date_published: null,
  content: {
    defaultCleaner: false,
    selectors: ['#article-body'],
    transforms: {
      '#article-body': ($node, $) => {
        const nodeImg = $('#header-container>div>div.image').find('figure');
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: ['.newsletter', '#continue_button', '.comments-container'],
  },
};
