// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const TheusunExtractor = {
  domain: 'www.the-sun.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['div.article__content'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [],
  },
};
