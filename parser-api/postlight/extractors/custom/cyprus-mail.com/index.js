export const CyprusmailExtractor = {
  domain: 'cyprus-mail.com',

  author: null,

  date_published: null,

  content: {
    // defaultCleaner: false,
    selectors: ['article'],

    transforms: {},
    clean: [
      'header',
      'footer',
      '.penci-entry-footer',
      '.entry-header.penci-entry-header.penci-title-',
    ],
  },
};
