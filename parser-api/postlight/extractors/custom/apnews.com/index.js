// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ApnewsExtractor = {
  domain: 'apnews.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['div.Article'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [],
  },
};
