// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const RoadandtrackExtractor = {
    domain: 'www.roadandtrack.com',
    author: null,
    date_published: null,
    content: {
        selectors: ['.standard-body'],
        transforms: {
            'iframe': $node => {
                $node.attr('width', '100%');
                $node.attr('height', '500px');
            },
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
        },
        clean: ['.spotim-module','.end-of-content-playlist'],
    },
};
