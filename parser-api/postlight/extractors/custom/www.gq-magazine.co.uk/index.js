// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const GqMagazineExtractor = {
  domain: 'www.gq-magazine.co.uk',
  author: null,
  date_published: null,
  content: {
    selectors: [
      'div[data-testid="ArticlePageChunks"]',
      'div[data-attribute-verso-pattern="gallery-body"]',
    ],
    transforms: {
      'div[data-testid="ArticlePageChunks"]': ($node, $) => {
        const nodeParent = $(
          'div[data-testid="ContentHeaderLeadAsset"]>figure'
        ).find('img');
        if (nodeParent && nodeParent.length > 0) {
          $node.prepend(nodeParent[0]);
        }
      },
      'div[data-attribute-verso-pattern="gallery-body"]': ($node, $) => {
        console.log($node);
        const nodeParent = $('header[data-testid="ContentHeader"]').find(
          'picture>img'
        );
        if (nodeParent && nodeParent.length > 0) {
          $node.prepend(nodeParent[0]);
        }
      },
      'figure>div[orientation="landscape"]': $node => {
        const nodeReplace = $node.find('picture');
        if (nodeReplace && nodeReplace.length > 0)
          $node.replaceWith(nodeReplace[0]);
      },

      '.CaptionWrapper-brWaob': 'figcaption',
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [],
  },
};
