// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const NewsweekExtractor = {
    domain: 'www.newsweek.com',
    author: null,
    date_published: null,
    content: {
        selectors: ['div.article-body.v_text','div.podcast-bl'],
        transforms: {
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
        },
        clean: [],
    },
};
