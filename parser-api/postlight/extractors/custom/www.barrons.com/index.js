// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const BarronsExtractor = {
  domain: 'www.barrons.com',
  supportedDomains: ['www.marketwatch.com'],
  author: null,
  date_published: null,
  content: {
    selectors: ['div#js-article__body', 'div[itemprop="articleBody"]'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      'figure.media-object-video.article__inset__video.media-object-video--standard': $node => {
        const divVideo = $node.find(
          'div.video-container.article__inset__video__player'
        );
        if (divVideo.length > 0) {
          const id = divVideo.attr('id');
          const dataSrc = divVideo.attr('data-src');
          if (id) {
            const $videoContainer = $(
              `<iframe width="100%" height="350" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"  src="https://video-api.wsj.com/api-video/player/v3/iframe.html?guid=${id}&shareDomain=http://www.barrons.com"></iframe>`
            );
            $node.replaceWith($videoContainer);
          } else if (dataSrc) {
            const $videoContainer = $(
              `<iframe  width="100%" height="350" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"  src="https://video-api.wsj.com/api-video/player/v3/iframe.html?guid=${dataSrc}&shareDomain=http://www.barrons.com"></iframe>`
            );
            $node.replaceWith($videoContainer);
          }
        }
      },
      'div#js-article__body': ($node, $) => {
        const nodeImg = $('.article__figure').find('figure');
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
    },
    clean: ['div.media-object-article-reader'],
  },
};
