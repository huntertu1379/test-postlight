// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const TimeExtractor = {
  domain: 'time.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['#article-body'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      '#article-body': ($node, $) => {
        const nodeVideo = $('.partial.lead-video>div.component.video-jw');
        if (nodeVideo && nodeVideo.length > 0) {
          const videoId = nodeVideo[0].attr('data-media-id');
          if (videoId)
            $node.prepend(
              $(
                `<iframe allowfullscreen frameborder="0" width="100%" height="450px" scrolling="no" id="molvideoplayer" src="https://cdn.jwplayer.com/previews/${videoId}"></iframe`
              )
            );
        }
        const nodeFigure = $('.partial.lead-image').find('.partial.figure');
        if (nodeFigure && nodeFigure.length > 0) {
          $node.prepend(nodeFigure[0]);
        }
      },
      'div.component.video-jw': ($node, $) => {
        const videoId = $node.attr('data-media-id');
        if (videoId)
          $node.replaceWith(
            $(
              `<iframe allowfullscreen frameborder="0" width="100%" height="450px" scrolling="no" id="molvideoplayer" src="https://cdn.jwplayer.com/previews/${videoId}"></iframe`
            )
          );
      },
    },
    clean: [],
  },
};
