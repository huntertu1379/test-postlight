// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const FtExtractor = {
  domain: 'www.ft.com',
  author: null,
  date_published: null,
  content: {
    defaultCleaner: false,
    selectors: ['.row__details.details'],
    transforms: {
      '.row__details.details': ($node, $) => {
        const nodeImg = $('.main-images.main__main-images').find('img');
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [
      '.placeholder',
      '.hidden',
      '.details__read-more',
      '.generic-article__body.article-details-type--newsletter.content--newsletter',
      'svg',
      '.comment',
    ],
    allowMultiple: true,
  },
};
