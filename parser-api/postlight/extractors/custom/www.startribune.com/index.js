// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const StartribuneExtractor = {
  domain: 'www.startribune.com',
  supportedDomains: ['video.startribune.com'],
  author: null,
  date_published: null,
  content: {
    selectors: ['.article-body', '.l-article-body'],
    transforms: {
      '.article-body': ($node, $) => {
        console.log(123);
        const nodeImg = $('.article-featured-media-wrapper.is-photo').find(
          'img'
        );
        const nodeVideo = $('.article-featured-media-wrapper.is-video').find(
          'iframe'
        );
        if (nodeImg && nodeImg.length > 0) $node.prepend(nodeImg[0]);
        else if (nodeVideo && nodeVideo.length > 0) $node.prepend(nodeVideo[0]);
      },
      'div.related-media.size-m': $node => {
        const nodeImg = $node.find('.expand-icn-wrapper').find('img');
        if (nodeImg && nodeImg.length > 0) {
          $node.replaceWith(nodeImg[0]);
        }
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      iframe: $node => {
        $node.attr('width', '100%');
        $node.attr('height', '500px');
      },
    },
    clean: ['.stack', '.l-article-downpage-left'],
  },
};
