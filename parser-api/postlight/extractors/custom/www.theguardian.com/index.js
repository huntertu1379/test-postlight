export const WwwTheguardianComExtractor = {
  domain: 'www.theguardian.com',

  title: {
    selectors: ['.content__headline'],
  },

  author: {
    selectors: ['p.byline'],
  },

  date_published: {
    selectors: [['meta[name="article:published_time"]', 'value']],
  },

  dek: {
    selectors: ['.content__standfirst'],
  },

  lead_image_url: {
    selectors: [['meta[name="og:image"]', 'value']],
  },

  content: {
    selectors: ['div#liveblog-body', 'div#maincontent'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      'div#liveblog-body': ($node, $) => {
        const prepenNode = $('div.dcr-qxav3n').find('img');
        if (prepenNode && prepenNode.length > 0) $node.prepend(prepenNode[0]);
      },
      'figure.dcr-10khgmf': $node => {
        const imgNode = $node.find('img');
        const videNode = $node.find('iframe');
        if (imgNode.length > 0) $node.replaceWith($(imgNode));
        else if (videNode.length > 0) {
          videNode.attr('width', '100%');
          $node.replaceWith($(videNode));
        }
      },
      'div#maincontent': ($node, $) => {
        const prepenNode = $('div[data-gu-name="media"]').find('img');
        if (prepenNode && prepenNode.length > 0) $node.prepend(prepenNode[0]);
        else {
          const prepenNode2 = $('header.dcr-1rm7u2e').find('img');
          if (prepenNode2 && prepenNode2.length > 0)
            $node.prepend(prepenNode2[0]);
        }
      },
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: [
      '.hide-on-mobile',
      '.inline-icon',
      'div#key-events-carousel',
      'div[data-testid]',
      'aside.dcr-z79tmx',
      'input',
      'svg',
      'div.dcr-1rw5kql',
    ],
  },
};
