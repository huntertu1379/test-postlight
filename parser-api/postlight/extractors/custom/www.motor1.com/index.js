// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const Motor1Extractor = {
  domain: 'www.motor1.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['.postContent'],
    transforms: {
      '.postContent': ($node, $) => {
        const nodeImg = $('.articleHeader').find('img');
        const nodeVideo = $('.videoBoxArticle').find('iframe');
        const nodeVideoImg = $('.videoBoxArticle').find('img');

        if (nodeImg && nodeImg.length > 0) $node.prepend(nodeImg[0]);
        else if (nodeVideo && nodeVideo.length > 0) $node.prepend(nodeVideo[0]);
        else if (nodeVideoImg && nodeVideoImg.length > 0)
          $node.prepend(nodeVideoImg[0]);
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: ['.lSSlideOuter '],
  },
};
