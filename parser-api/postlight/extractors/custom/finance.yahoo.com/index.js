// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const FinanceExtractor = {
  domain: 'finance.yahoo.com',
  author: {
    selectors: ['div.caas-attr-item-author'],
  },

  date_published: {
    selectors: [['time', 'datetime']],
  },
  title: {
    selectors: ['header.caas-title-wrapper'],
  },

  content: {
    selectors: ['div.caas-content-wrapper'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      // 'header.caas-title-wrapper':"div"
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: [],
  },

  dek: {
    selectors: [],
  },

  next_page_url: {
    selectors: [
      // enter selectors
    ],
  },

  excerpt: {
    selectors: [
      // enter selectors
    ],
  },
};
