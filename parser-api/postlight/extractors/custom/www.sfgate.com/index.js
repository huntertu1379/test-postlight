// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const SfgateExtractor = {
  domain: 'www.sfgate.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['article.article--content'],
    transforms: {
      '.article--content': ($node, $) => {
        const prepenNode = $('div.articleHeader--image.image').find('picture');
        if (prepenNode && prepenNode.length > 0) $node.prepend(prepenNode[0]);
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: ['.story-card.manual', '.article--content-embed'],
  },
};
