export const TwitterExtractor = {
  domain: 'twitter.com',
  content: {
    transforms: {
      'div[data-testid="videoPlayer"]': $node => {
        const $tweetContainer = $(`<blockquote class="twitter-tweet"><a href="${window.location.href}"></a></blockquote>
        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>`);
        $node.replaceWith($tweetContainer);
        console.log($node);
      },
      'article[data-testid="tweet"][tabindex="-1"]': $node => {
        const time = $node.find('time[datetime]')[0];
        if (time) {
          time.setAttribute('class', 'date_publishedCs');
        }
      },
      img: $node => {
        const src = $node.attr('src');
        if (src.includes('/emoji/')) {
          $node.attr('width', '28px');
          $node.attr('height', '28px');
        }
      },
    },

    selectors: ['article[data-testid="tweet"][tabindex="-1"]'],

    defaultCleaner: false,

    clean: [
      'div[role="group"]',
      'div[data-testid="User-Names"]',
      'svg',
      'div[data-testid="Tweet-User-Avatar"]',
    ],
  },

  author: {
    selectors: ['div[data-testid="User-Names"]'],
    allowMultiple: true,
  },
  date_published: {
    selectors: ['.date_publishedCs'],
  },
};
