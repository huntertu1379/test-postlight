// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const News18Extractor = {
  domain: 'www.news18.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['#article_ContentWrap'],
    transforms: {
      '#article_ContentWrap': ($node, $) => {
        const nodeImg = $('.article_bimg').find('img');
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
      iframe: $node => {
        const dataSrc = $node.attr('data-src');
        if (dataSrc) $node.attr('src', dataSrc);
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [],
  },
};
