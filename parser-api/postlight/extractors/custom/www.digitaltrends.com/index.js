// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const DigitaltrendsExtractor = {
    domain: 'www.digitaltrends.com',
    author: null,
    date_published: null,
    content: {
        selectors: ['#dt-post-content', 'article[itemprop="articleBody"]'],
        transforms: {
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
        },
        clean: ['h4.b-editors-recs-title.h-editors-recs-title'],
    },
};
