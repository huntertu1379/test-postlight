// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const HuffpostExtractor = {
  domain: 'www.huffpost.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['section#entry-body'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [
      'aside.cli.manual.cli-related-articles.js-cet-subunit',
      'div.cli.cli-embed.js-no-inject',
    ],
  },
};
