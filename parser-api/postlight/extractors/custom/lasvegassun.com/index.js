export const LasvegassunExtractor = {
  domain: 'lasvegassun.com',

  author: null,

  date_published: null,

  content: {
    defaultCleaner: false,
    selectors: [
      '.article',
      'div[itemprop="articleBody"]',
      '#multimediaContainer',
    ],

    transforms: {
      '#multimediaContainer': $node => {
        const appenNode = document.querySelector('#multimediaMeta');
        if (appenNode) $node.append($(appenNode.outerHTML));
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [
      '.bypubdate-original',
      '.bypubdate-updated',
      '.galleria-bar',
      '.galleria-tooltip',
      '.bypubdate',
    ],
  },
};
