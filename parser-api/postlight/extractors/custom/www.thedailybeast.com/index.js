// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ThedailybeastExtractor = {
  domain: 'www.thedailybeast.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['article.Body.hpCCr'],
    transforms: {
      '.Body__content': ($node, $) => {
        const prepenNode = $(
          'div.StandardHeader.StandardHeader--single-author'
        ).find('picture');
        if (prepenNode && prepenNode.length > 0) $node.prepend(prepenNode[0]);
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [],
    allowMultiple: true,
  },
};
