// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const NewsMitExtractor = {
  domain: 'news.mit.edu',
  author: {
    selectors: ['.news-article--author'],
  },
  date_published: {
    selectors: [['time[datetime]', 'datetime']],
  },
  content: {
    selectors: ['.news-article--content'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      '.news-article--content': ($node, $) => {
        const nodeImg = $('.news-article--media--image--file>img');
        if (nodeImg && nodeImg.length > 0) $node.prepend(nodeImg[0]);
      },
    },
    clean: [],
  },
};
