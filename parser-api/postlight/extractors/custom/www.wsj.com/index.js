// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const WsjExtractor = {
  domain: 'www.wsj.com',
  author: {
    selectors: [['meta[name="author"]', 'content']],
  },
  date_published: {
    selectors: [['meta[itemprop="datePublished"]', 'value']],
  },
  content: {
    selectors: ['section.css-1ducvg2-Container.e1d75se20'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      'div[data-inset_type]': $node => {
        if ($node.attr('data-type') === 'video') {
          const divVideo = $node.find(
            'div.VideoPlayer__VideoPlayerContainer-sc-1eakzhs-0.fhzaIK.video-player'
          );
          if (divVideo) {
            const videoId = divVideo.attr('id').substring(5);
            const $videoContainer = $(
              `<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"  width="100%" height="350" src="https://video-api.wsj.com/api-video/player/v3/iframe.html?guid=${videoId}"></iframe>`
            );
            $node.replaceWith($videoContainer);
          }

        } else {
          const $children = $node.children();
          if ($children.length >= 1 && $children.get(0).tagName === 'FIGURE') {
            $node.replaceWith($children.get(0));
          }
        }
      },
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: [],
  },

  dek: {
    selectors: [],
  },

  next_page_url: {
    selectors: [
      // enter selectors
    ],
  },

  excerpt: {
    selectors: [
      // enter selectors
    ],
  },
};
