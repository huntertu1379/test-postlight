// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const DailymailExtractor = {
  domain: 'www.dailymail.co.uk',
  author: null,
  date_published: null,
  content: {
    selectors: ['div[itemprop="articleBody"]'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      'div.artSplitter.mol-img-group': $node => {
        const img = $node.find('img');
        const caption = $node.find('p.imageCaption');
        if (img.length > 0) {
          const src = img.attr('src');
          $node.replaceWith(
            `<figure><img src="${src}" width="100%" height="auto"/><figcaption>${caption.text()}</figcaption></figure>`
          );
        }
      },
      'div.moduleFull.mol-video': $node => {
        const dataVideo = $node.find('.vjs-video-container>div[data-opts]');
        if (dataVideo && dataVideo.length > 0) {
          const urlEmbed = JSON.parse(dataVideo.attr('data-opts'))['plugins']['social-share']['embedUrl'];
          if (urlEmbed)
            $node.replaceWith(
              `<iframe allowfullscreen frameborder="0" width="100%" height="450px" scrolling="no" id="molvideoplayer" src="${urlEmbed}"></iframe`
            );
        }
      },
    },
    clean: ['.mol-factbox'],
  },
};
