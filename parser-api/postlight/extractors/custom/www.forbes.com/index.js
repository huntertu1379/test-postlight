// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ForbesExtractor = {
  domain: 'www.forbes.com',
  author: {
    selectors: [['meta[itemprop="author"]', 'content']],
  },
  date_published: {
    selectors: [['meta[itemprop="datePublished"]', 'content']],
  },

  content: {
    selectors: ['div.article-body-container.show-iframes'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {},

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: ['div.article-footer', 'cnx'],
  },

  dek: {
    selectors: [],
  },

  next_page_url: {
    selectors: [
      // enter selectors
    ],
  },

  excerpt: {
    selectors: [
      // enter selectors
    ],
  },
};
