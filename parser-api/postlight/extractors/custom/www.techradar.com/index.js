// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const TechradarExtractor = {
  domain: 'www.techradar.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['#article-body'],
    transforms: {
      '#article-body': ($node, $) => {
        const nodeImg = $('.hero-image-padding').find('picture>img');
        const nodeImg2 = $('div[itemprop="image"]').find('picture>img');
        if (nodeImg && nodeImg.length > 0) $node.prepend(nodeImg[0]);
        else if (nodeImg2 && nodeImg2.length > 0) $node.prepend(nodeImg2[0]);
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      'div.hawk-nest.hawk-processed': $node => {
        const nodeDelete = $node.find('div[title="Latest news"]');
        if (nodeDelete && nodeDelete.length > 0) $node.remove();
        const nodeData = $node.attr('data-widget-introduction');
        const nodeImage = $node.attr('data-image');
        $node.replaceWith(
          `<div><img src="${nodeImage ||
          ''}" width="140px" height="160px" /><div> ${nodeData ||
          ''}</div></div>`
        );
      },
    },
    clean: ['svg', 'aside', '.fancy-box', '#affiliate-disclaimer'],
  },
};
