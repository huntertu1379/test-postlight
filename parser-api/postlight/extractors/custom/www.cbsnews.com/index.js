// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const CbsnewsExtractor = {
  domain: 'www.cbsnews.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['section#post-intro', 'section.content__body'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      'figure.embed.embed--type-video.embed--type-content_video.is-video.embed--float-none.embed--size-medium': $node => {
        const divVideo = $node
          .find('div.embed__content-wrapper')
          .find('iframe');
        if (divVideo.length > 0) {
          const dataSrc = divVideo.attr('data-src');
          if (dataSrc) {
            divVideo.attr('src', dataSrc);
            $node.replaceWith(divVideo);
          }
        }
      },
      'figure.embed.embed--type-video.embed--type-content_video.is-video.embed--float-left.embed--size-medium': $node => {
        const divVideo = $node
          .find('div.embed__content-wrapper')
          .find('iframe');
        if (divVideo.length > 0) {
          const dataSrc = divVideo.attr('data-src');
          if (dataSrc) {
            divVideo.attr('src', dataSrc);
            $node.replaceWith(divVideo);
          }
        }
      },
      'figure.embed.embed--type-video.embed--type-content_video.is-video.embed--float-right.embed--size-medium': $node => {
        const divVideo = $node
          .find('div.embed__content-wrapper')
          .find('iframe');
        if (divVideo.length > 0) {
          const dataSrc = divVideo.attr('data-src');
          if (dataSrc) {
            divVideo.attr('src', dataSrc);
            $node.replaceWith(divVideo);
          }
        }
      },
    },
    clean: [
      'aside.component.list.recirculation.component--type-recirculation',
      'ul.content__tags',
      'figcaption',
    ],
  },
};
