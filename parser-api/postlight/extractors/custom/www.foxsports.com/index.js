// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const FoxsportsExtractor = {
  domain: 'www.foxsports.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['div.story-content'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      'ul.mg-t-b-20.article-content': $node => {
        const previousNode = $node.prev();
        if (previousNode.length > 0 && previousNode.children('strong'))
          previousNode.remove();
        $node.remove();
      },
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: ['div.story-favorites-section-add'],
  },

  dek: {
    selectors: [],
  },

  next_page_url: {
    selectors: [
      // enter selectors
    ],
  },

  excerpt: {
    selectors: [
      // enter selectors
    ],
  },
};
