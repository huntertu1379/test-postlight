export const BuzzfeednewsExtractor = {
  domain: 'www.buzzfeednews.com',

  author: null,

  date_published: null,

  content: {
    defaultCleaner: false,
    selectors: ['.subbuzzes--buzzfeed_news', 'article'],

    transforms: {},
    clean: [
      '.subbuzz-bfp--custom_embed',
      '.subbuzz-bfp--related_links',
      '.subbuzz__figure-footer',
    ],
  },
};
