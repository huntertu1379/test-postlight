// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ThehillExtractor = {
  domain: 'thehill.com',
  author: null,
  date_published: null,
  content: {
    selectors: [
      'div.col-main.flow.page-content.overflow-hidden.font-base.text-400',
    ],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [
      'section.mobile-only',
      'figcaption',
      'aside',
      'section.text-300.text-transform-upper',
      'section.social.header__meta.text-transform-upper.weight-semibold.font-base',
      'section.ad-unit.ad-unit--leader-mr2',
      'section.extended-scroll',
    ],
  },
};
