// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const FreecartokTistoryExtractor = {
  domain: 'freecartok.tistory.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['.article_view'],
    transforms: {
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
      iframe: $node => {
        $node.attr('width', '100%');
        $node.attr('height', '500px');
      },
    },
    clean: ['.stack', '.l-article-downpage-left'],
  },
};
