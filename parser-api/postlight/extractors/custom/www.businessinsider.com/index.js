// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const BusinessinsiderExtractor = {
  domain: 'www.businessinsider.com',
  supportedDomains: ['www.insider.com'],

  author: {
    selectors: [['meta[property="author"]', 'content']],
  },
  date_published: {
    selectors: [['meta[name="datePublished"]', 'content']],
  },
  content: {
    selectors: ['article'],
    transforms: {},
    clean: [
      'section.post-content-more',
      'section.category-wrapper.headline-regular',
      'div.the-refresh-player-wrapper.d-none.d-lg-block',
      'section.post-content-bottom.taboola-feed',
      'sticky-footer-ad',
      '.notification-prompt-wrapper',
    ],
  },
};
