// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const UsatodayExtractor = {
  domain: 'www.usatoday.com',
  author: {
    selectors: ['h1.gnt_ar_hl'],
  },

  content: {
    selectors: ['div.gnt_ar_b'],

    // Is there anything in the content you selected that needs transformed
    // before it's consumable content? E.g., unusual lazy loaded images
    transforms: {
      'div.gnt_ar_b': $node => {
        const childNode = $node.children(
          'aside.gnt_em.gnt_em__fp.gnt_em_vp__tp.gnt_em__el'
        );
        if (childNode.length > 0) {
          const videoId = JSON.parse(
            childNode
              .find('button.gnt_em_vp_a.gnt_em_vp__tp_a')
              .attr('data-c-vpdata')
          ).id;
          const $videoContainer = $(
            `<iframe title="USATODAY-Embed Player" width="100%" height="350" frameborder="0" scrolling="no" allowfullscreen="true" marginheight="0" marginwidth="0" src="https://uw-media.usatoday.com/embed/video/${videoId}?placement=snow-embed"></iframe>`
          );
          childNode.replaceWith($videoContainer);
        }
      },
    },

    // Is there anything that is in the result that shouldn't be?
    // The clean selectors will remove anything that matches from
    // the result
    clean: [],
  },

  dek: {
    selectors: [],
  },

  next_page_url: {
    selectors: [
      // enter selectors
    ],
  },

  excerpt: {
    selectors: [
      // enter selectors
    ],
  },
};
