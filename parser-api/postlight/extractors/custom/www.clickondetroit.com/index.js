// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ClickondetroitExtractor = {
  domain: 'www.clickondetroit.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['section[itemprop="articleBody"]'],
    transforms: {
      'section[itemprop="articleBody"]': ($node, $) => {
        const nodeParent = $('.basicStory');
        if (nodeParent && nodeParent.length > 0) {
          const nodePrepen = nodeParent[0].firstChild;
          if (nodePrepen.tagName == 'FIGURE') $node.prepend(nodePrepen);
          else if (nodePrepen.tagName == 'DIV') {
            const nodeScript = nodePrepen.find('script[data-loaded="true"]');
            if (nodeScript && nodeScript.length > 0)
              $node.prepend(nodeScript[0]);
          }
        }
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: [],
  },
};
