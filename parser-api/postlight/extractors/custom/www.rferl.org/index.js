export const RferlExtractor = {
  domain: 'www.rferl.org',

  author: null,

  date_published: null,

  content: {
    defaultCleaner: false,
    selectors: ['#newsItems > div:nth-child(1)', '.col-multimedia'],
    transforms: {
      '.c-sticky-container': $node => {
        const videoId = $node.find('video');
        if (videoId && videoId.attr('data-sdkid'))
          $node.html(
            `<iframe src="https://www.rferl.org/embed/player/0/${videoId.attr('data-sdkid')}.html?type=video" frameborder="0" scrolling="no" width="640" height="360" allowfullscreen></iframe>`
          );
      },
    },
    clean: [
      '.publishing-details',
      '.news__buttons',
      'input',
      '.news__share-outer',
      '.news__read-more',
      '.share__list',
      '.c-author',
    ],
  },
};
