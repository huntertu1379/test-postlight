// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ThehinduExtractor = {
  domain: 'www.thehindu.com',
  author: null,
  date_published: null,
  content: {
    selectors: ['.articlebodycontent'],
    transforms: {
      '.articlebodycontent': ($node, $) => {
        const nodeImg = $('.article-picture.top-pic').find('picture');
        if (nodeImg && nodeImg.length > 0) {
          $node.prepend(nodeImg[0]);
        }
      },
      img: $node => {
        $node.attr('width', '100%');
        $node.attr('height', 'auto');
      },
    },
    clean: ['.comments', '.share.share-text'],
  },
};
