// Rename CustomExtractor
// to fit your publication
// (e.g., NYTimesExtractor)
export const ExpressExtractor = {
    domain: 'www.express.co.uk',
    author: null,
    date_published: null,
    content: {
        defaultCleaner: false,
        selectors: ['.ctx_content', 'div[data-type="article-body"]'],
        transforms: {
            'img': $node => {
                $node.attr('width', '100%');
                $node.attr('height', 'auto');
            },
            'div.jw-player-container': ($node, $) => {
                const nodeVideo = $node.find('div[aria-label="Video Player"]')
                if (nodeVideo && nodeVideo.length > 0) {
                    const videoId = nodeVideo[0].getAttribute('id').split('_')
                    if (videoId && videoId.length > 2)
                        $node.replaceWith($(`<iframe allowfullscreen frameborder="0" width="100%" height="450px" scrolling="no" id="molvideoplayer" src="https://cdn.jwplayer.com/previews/${videoId[1]}"></iframe`))
                }
            }
        },
        clean: ['.newsletter-pure', 'source', '.clear'],
    },
};
