import { normalizeSpaces } from '../utils/text/index.js';
import { CLEAN_AUTHOR_RE } from './constants.js';

// Take an author string (like 'By David Smith ') and clean it to
// just the name(s): 'David Smith'.
export default function cleanAuthor(author) {
  return author && author.length > 0 && Array.isArray(author) ?
    normalizeSpaces(author[0].replace(CLEAN_AUTHOR_RE, '$2').trim()) :
    normalizeSpaces(author.replace(CLEAN_AUTHOR_RE, '$2').trim());
}
