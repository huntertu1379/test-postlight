import {
  corsSuccessResponse,
  corsErrorResponse,
  runWarm,
} from './utils/index.ts';

const fs = require('fs');

const checkDomain = async ({ queryStringParameters }, context, cb) => {
  let isCustom = false;
  const { domain } = queryStringParameters;
  await fs.promises
    .readFile('./postlight/extractors/custom/index.js')
    .then(async function(result) {
      if (result && result.toString().includes(domain)) {
        isCustom = true;
      }
    })
    .catch(function(error) {
      console.log(`${error}`);
      return cb(
        null,
        corsErrorResponse({ message: 'There was an error parsing that URL.' })
      );
    });

  return cb(null, corsSuccessResponse(isCustom));
};

export default runWarm(checkDomain);
