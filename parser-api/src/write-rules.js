import {
  corsSuccessResponse,
  corsErrorResponse,
  runWarm,
} from './utils/index.ts';

const fs = require('fs');

const writeRules = async ({ body }, context, cb) => {
  try {
    const rules = JSON.parse(body);
    console.log(rules);
    if (!rules || !rules.domain || !rules.rulesCustom)
      return cb(null, corsErrorResponse('Body illegal!'));
    const pathWrite = `./postlight/extractors/custom/${rules.domain}`;
    if (!fs.existsSync(pathWrite)) {
      await fs.promises.mkdir(pathWrite, error =>
        error
          ? console.log(error)
          : console.log('You have created the target_directory')
      );
    }

    await fs.promises
      .readFile('./postlight/extractors/custom/index.js')
      .then(async function(result) {
        await fs.promises.writeFile(`${pathWrite}/index.js`, rules.rulesCustom);
        if (result && !result.toString().includes(rules.domain)) {
          fs.appendFileSync(
            './postlight/extractors/custom/index.js',
            `\nexport * from './${rules.domain}'`
          );
        }
      })
      .catch(function(error) {
        console.log(error);
      });

    return cb(null, corsSuccessResponse({ status: 200, message: 'success!' }));
  } catch (err) {
    return cb(null, corsErrorResponse({ status: 500, message: err }));
  }
};

export default runWarm(writeRules);
